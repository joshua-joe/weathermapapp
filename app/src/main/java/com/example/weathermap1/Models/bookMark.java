package com.example.weathermap1.Models;

public class bookMark {
    String name,lattd,longttd;
    int id;
    public static final String TABLE_NAME = "bookmarkTable";

    public static final String COLUMN_ID = "id";
    public static final String NAME = "name";
    public static final String LAT = "latitude";
    public static final String LONGD ="longitude";

    public bookMark(String  name, String lattd, String longttd){
        this.name = name;
        this.lattd = lattd;
        this.longttd = longttd;
    }
    public bookMark(int id, String name, String lattd ,String longttd){
        this.id = id;
        this.name = name;
        this.lattd = lattd;
        this.longttd = longttd;
    }
    public bookMark(){}
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + " ("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + NAME + " TEXT, "
                    + LAT + " TEXT, "
                    + LONGD + " TEXT"
                    + ");";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLattd() {
        return lattd;
    }

    public void setLattd(String lattd) {
        this.lattd = lattd;
    }

    public String getLongttd() {
        return longttd;
    }

    public void setLongttd(String longttd) {
        this.longttd = longttd;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
