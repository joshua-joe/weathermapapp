package com.example.weathermap1;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.weathermap1.Models.bookMark;
import com.example.weathermap1.db.DatabaseHelper;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MyListAdapter extends RecyclerView.Adapter<MyListAdapter.ViewHolder>{
    private ArrayList<bookMark> listdata;
    private DatabaseHelper dbhelper;
    Context context;
    private RequestQueue mQueue,mQueue2;

    ArrayList alist;

    // RecyclerView recyclerView;
    public MyListAdapter(Context context,ArrayList<bookMark> listdata) {
        this.listdata = listdata;
        this.context = context;
        dbhelper = new DatabaseHelper(context);
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final bookMark myListData = listdata.get(position);

        holder.tvName.setText(myListData.getName());
        holder.tvLat.setText(myListData.getLattd());
        holder.tvLong.setText(myListData.getLongttd());
        holder.btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbhelper.deleteNote(myListData);
                holder.relativeLayout.setVisibility(View.GONE);
            }
        });
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView tvCurrentWeather,tvFiveDayWeather;
                LinearLayout weatherResult;
                weatherResult= view.findViewById(R.id.weatherResult_id);
                weatherResult.setVisibility(View.VISIBLE);
                tvCurrentWeather  = view.findViewById(R.id.currentWeather_id);
                tvFiveDayWeather = view.findViewById(R.id.fiveDayWeather_id);

                mQueue = Volley.newRequestQueue(context);
                mQueue2 = Volley.newRequestQueue(context);
                String lats= myListData.getLattd();
                String lons= myListData.getLongttd();

                String url = "http://api.openweathermap.org/data/2.5/weather?lat="+lats+"&lon="+lons+"&cnt=1&units=metric&appid=3ccb6dbfee3a12453e852a1fed24e374";
                JsonObjectRequest request = new JsonObjectRequest(com.android.volley.Request.Method.GET, url, null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    JSONObject main1 = response.getJSONObject("main");
                                    JSONObject wind = response.getJSONObject("wind");
                                    //String rain = response.getString("rain");

                                    Log.d("TempTest: ", main1.getString("temp") + main1.getString("humidity")
                                            + wind.getString("speed") + wind.getString("deg"));


                                    String temp = main1.getString("temp");
                                    String humidiy = main1.getString("humidity");
                                    String winds = "Speed: " + wind.getString("speed") + ", Deg: " + wind.getString("deg");
                                    tvCurrentWeather.append("Tempereture: "+temp+"\n"
                                            +"Humidity: "+humidiy+"\n"
                                            +"wind: "+winds+"\n"
                                            +"Rain: "+null+"\n\n\n "+"Five Day ForeCast \n");

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });
                mQueue.add(request);

//*********===================>>>>>>>

                String url2 = "http://api.openweathermap.org/data/2.5/forecast?lat="+lats+"&lon="+lons+"&cnt=5&units=metric&appid=3ccb6dbfee3a12453e852a1fed24e374";
                JsonObjectRequest request2 = new JsonObjectRequest(com.android.volley.Request.Method.GET, url2, null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    JSONArray jsonArray = response.getJSONArray("list");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject object = jsonArray.getJSONObject(i);
                                    JSONObject main = object.getJSONObject("main");
                                    JSONObject wind1 = object.getJSONObject("wind");
                                    String date = object.getString("dt_txt");
                                    //String rain1 = response.getString("rain");

                                    Log.d("TempTest: ", main.getString("temp") + main.getString("humidity")
                                            + wind1.getString("speed") + wind1.getString("deg"));


                                    String temp = main.getString("temp");
                                    String humidiy = main.getString("humidity");
                                    String winds = "Speed: " + wind1.getString("speed") + ", Deg: " + wind1.getString("deg");

                                    tvFiveDayWeather.append(
                                            "Tempereture: "+temp+"\n"
                                            +"Humidity: "+humidiy+"\n"
                                            +"wind: "+winds+"\n"
                                            +"Rain: "+null+"\n\n");
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });
                mQueue2.add(request2);
    }}

    );}

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        public TextView tvLat;
        public TextView tvLong;
        public RelativeLayout relativeLayout;
        public Button btnRemove;
        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.bmTitle);
            tvLat = itemView.findViewById(R.id.bmLattd);
            tvLong = itemView.findViewById(R.id.bmLongtd);
            relativeLayout =itemView.findViewById(R.id.relativeLayout);
            btnRemove = itemView.findViewById(R.id.delete);
        }
    }
}
