package com.example.weathermap1;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.weathermap1.Models.bookMark;
import com.example.weathermap1.db.DatabaseHelper;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends FragmentActivity implements OnMapReadyCallback {
private LinearLayout adjustviews ;
private SearchView searchRelView;
private RecyclerView recyclerView;

private static ArrayList mCurrentWeather ;
  static   AlertDialog.Builder nBuilder;
    GoogleMap mMap;
    AlertDialog.Builder builder;
    ArrayList<bookMark> list;
    private DatabaseHelper db;
    MyListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        builder = new AlertDialog.Builder(this);
        nBuilder=new AlertDialog.Builder(this);

        list =new ArrayList<>();
        db = new DatabaseHelper(this);
        adjustviews = findViewById(R.id.llone_id);
        searchRelView = findViewById( R.id.sv_location);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
        .findFragmentById(R.id.map);
        searchRelView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Address addresses = null;
                String location = searchRelView.getQuery().toString();
                if (location!= null){
                    Geocoder geocoder = new Geocoder(MainActivity.this);
                    try {
                        addresses = geocoder.getFromLocationName(location,1).get(0);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    LatLng latLng = new LatLng(addresses.getLatitude(),addresses.getLongitude());
                    mMap.addMarker(new MarkerOptions().position(latLng).title(location));
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,10));
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        mapFragment.getMapAsync(this::onMapReady);

        init();
        help();

        recyclerView = findViewById(R.id.recView_id);
        list.addAll(db.getAllNotes());
        adapter = new MyListAdapter(this,list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

    }
    private void init(){
        Button button = findViewById(R.id.adBkMarkbtn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adjustviews.setVisibility(View.GONE);
                searchRelView.setVisibility(View.VISIBLE);

            }
        });
    }
    private void help(){
        Button buttonHelp = findViewById(R.id.helpbtn_id);
        buttonHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,HelpClass.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        mMap.getUiSettings().setZoomControlsEnabled(true);
       mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
           @Override
           public void onMapClick(LatLng latLng) {
               Log.d(this.getClass().getSimpleName(),latLng.latitude+":"+latLng.longitude);
               MarkerOptions markerOptions = new MarkerOptions();
               markerOptions.position(latLng);

               try{
                   Geocoder geocoder = new Geocoder(getApplicationContext(),Locale.getDefault());
               List<Address> address  = geocoder.getFromLocation(latLng.latitude,latLng.longitude,1);
               if (address!=null)
               markerOptions.title(address.get(0).getLocality()+latLng.latitude+":"+latLng.longitude);
                   alertDialog(latLng,address.get(0).getCountryName());
               mMap.clear();
               mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,5));
               mMap.addMarker(markerOptions);
               AlertDialog alertDialog = builder.create();
               alertDialog.show();
               }catch(Exception e){
                   e.printStackTrace();
               }
           }
       });
    }
    private void alertDialog(LatLng latLng ,String name){

        builder.setMessage("Do you want to bookmark this location ? ")
                .setCancelable(false)
                .setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int i = 0;
                        bookMark myObject = new bookMark("location: "+name,String.valueOf(latLng.latitude),String.valueOf(latLng.longitude));
                        createNote(myObject);
                        adjustviews.setVisibility(View.VISIBLE);
                        recreate();
                        Toast.makeText(MainActivity.this, "Location Added successfully", Toast.LENGTH_SHORT).show();
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
    }
    private void createNote(bookMark note) {
        // inserting note in db and getting
        // newly inserted note id
        db.insertNote(note);

        // get the newly inserted note from db
            // refreshing the list
            adapter.notifyDataSetChanged();

    }

    private static void toggleRefresh() {

    }

}